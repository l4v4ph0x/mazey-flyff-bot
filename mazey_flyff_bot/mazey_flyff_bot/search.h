#ifndef SEARCH_H
#define SEARCH_H

#include <windows.h>
#include <psapi.h>
#include <tlhelp32.h>


#define size_to_scan 786

unsigned long get_module(void *handle, const char *name) {
  HMODULE hMods[1024];
  DWORD cbNeeded;
  unsigned int i;

  if (EnumProcessModules(handle, hMods, sizeof(hMods), &cbNeeded)) {
    for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++) {
      TCHAR szModName[MAX_PATH];

      if (GetModuleFileNameEx(handle, hMods[i], szModName,
                              sizeof(szModName) / sizeof(TCHAR))) {

        unsigned int j;
        for (j = strlen(szModName) - 1; j > 0; j--) {
          if (szModName[j] == '\\') {
            j++;
            break;
          }
        }

        if (_stricmp(szModName + j, name) == 0) {
          return (unsigned long)hMods[i];
        }
      }
    }
  }

  return 0;
}

unsigned long get_proc(const char *name) {
  void *snapshot;
  PROCESSENTRY32 pe32;
  unsigned long ret;

  snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pe32.dwSize = sizeof(PROCESSENTRY32);

  while (Process32Next(snapshot, &pe32)) {
    if (_stricmp(pe32.szExeFile, name) == 0) {
      ret = pe32.th32ProcessID;
      break;
    }
  }

  CloseHandle(snapshot);
  return ret;
}

unsigned long search(void *handle, unsigned long start, unsigned long size,
                     const char *bytesToSearch, unsigned long sizeToSearch,
                     unsigned long step) {
  unsigned char bytes[size_to_scan];
  unsigned long gotCount = 0;

  for (unsigned long i = 0; i < size; i += (size_to_scan - sizeToSearch)) {
    ReadProcessMemory(handle, (PVOID)(start + i), &bytes, size_to_scan, 0);
    for (unsigned long j = 0; j < size_to_scan; j++) {
      bool failed = false;

      for (unsigned char k = 0; k < sizeToSearch; k++) {
        if (*(unsigned char *)((unsigned long)&bytes + j + k) !=
            (unsigned char)bytesToSearch[k]) {
          failed = true;
          break;
        }
      }

      if (failed == false) {
        gotCount++;
        if (gotCount == step)
          return start + i + j;
      }
    }
  }
  return 0;
}

#endif