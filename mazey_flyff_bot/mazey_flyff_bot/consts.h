#ifndef CONSTS_H
#define CONSTS_H

#include <vector>

const char projectName[] = "mazey_flyff_bot";

struct key {
  const char name[3];
  unsigned long code;
};

static std::vector<key> keys = {
  { "F1", 0x70 },
  { "F2", 0x71 },
  { "F3", 0x72 },
  { "F4", 0x73 },
  { "F5", 0x74 },
  { "F6", 0x75 },
  { "F7", 0x76 },
  { "F8", 0x77 },
};

static std::vector<unsigned long> reselect_after_times = {
    10, 20, 30, 40, 50, 60
};

#endif
